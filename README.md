# netdog

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Like netcat but using [Veilid](https://veilid.com/)

**note**: It's buggy. On some runs, I get an 'MustJoinHandle' error and once I think I never received the message on the server side.

## Demo
https://asciinema.org/a/rwn7zi09OElhy9M5YO80RXENo

## Usage

```sh
# on the receiving side
netdog --server

# on the sending side
netdog --client <blob>
```

'blob' is the blob from `veilid_core::VeilidAPI::new_private_route()` (and returned by `netdog --server` when the route is ready). Not just the `node_id`.
