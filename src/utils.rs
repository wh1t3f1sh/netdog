use anyhow::Error;
use tokio::time::{sleep, Duration};
use tracing::info;
use veilid_core::{AttachmentManager, AttachmentState, VeilidAPI};
use AttachmentState::{
    AttachedGood, AttachedStrong, AttachedWeak, Attaching, Detached, Detaching, FullyAttached,
    OverAttached,
};

pub async fn wait_for_attached(attachment_manager: &AttachmentManager) {
    info!("awaiting attachment");
    loop {
        match attachment_manager.get_attachment_state() {
            Detached | Attaching | Detaching => (),
            AttachedWeak | AttachedGood | AttachedStrong | FullyAttached | OverAttached => break,
        }
        sleep(Duration::from_millis(100)).await;
    }
    info!("awaiting attachment, done");
}

pub async fn wait_for_network_start(api: &VeilidAPI) {
    info!("awaiting network initialization");
    loop {
        match api.get_state().await {
            Ok(vs) => {
                if vs.network.started && !vs.network.peers.is_empty() {
                    info!(
                        "awaiting network initialization, done ({} peer(s))",
                        vs.network.peers.len()
                    );
                    break;
                }
            }
            Err(e) => {
                panic!("Getting state failed: {:?}", e);
            }
        }
        sleep(Duration::from_millis(100)).await;
    }
}

pub async fn wait_for_public_internet_ready(
    attachment_manager: &AttachmentManager,
) -> Result<(), Error> {
    info!("awaiting 'public_internet_ready'");
    loop {
        let state = attachment_manager.get_veilid_state();
        if state.public_internet_ready {
            break;
        }
        sleep(Duration::from_secs(5)).await;
    }
    info!("awaiting 'public_internet_ready', done");
    Ok(())
}
